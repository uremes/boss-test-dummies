import GPy
import matplotlib.pyplot as plt
import numpy as np


def load_model(filename):

    # load saved data
    saved = np.load(filename)
    dim = saved['X'].shape[1]

    # create kernel
    k = GPy.kern.StdPeriodic(input_dim=dim, ARD1=True, ARD2=True)

    # create mean function
    mf = GPy.mappings.Constant(dim, 1)

    # create model
    m = GPy.models.GPRegression(saved['X'], saved['Y'], kernel=k, mean_function=mf)

    # set model params
    m[:] = saved['params']
    m.fix()
    m.parameters_changed()

    return m


def plot_slices(f, xp, plot_point, levels=25):

    x1, x2 = np.meshgrid(xp, xp)
    dim = len(plot_point)
    for dim1 in range(dim-1):
        for dim2 in range(dim-dim1-1):
            plt.subplot(dim-1, dim-1, dim*dim1+dim2+1)
            plot_input = np.tile(plot_point, (len(xp)*len(xp), 1))
            plot_input[:, dim1] = x1.ravel()
            plot_input[:, dim1+dim2+1] = x2.ravel()
            yp = f(plot_input)
            cset = plt.contour(xp, xp, yp.reshape(len(xp), len(xp)), levels=levels)
            plt.colorbar(cset)
            plt.xlabel('dim {}'.format(dim1+1))
            plt.ylabel('dim {}'.format(dim1+dim2+2))
