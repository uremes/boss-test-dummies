# BOSS test dummies

Models that can be used to test [BOSS](https://gitlab.com/cest-group/boss) in an optimisation task that mimics atomistic structure search and example test runs.

- models: Models fitted on input-output data collected in past optimisation experiments at multiple fidelities.

- notebooks and command-line: Example notebooks and command-line input files focussed on multi-fidelity optimisation scenarios.

The examples have been tested with BOSS 1.10.1. This resource is not maintained.