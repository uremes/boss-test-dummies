import numpy as np
import GPy

def load_model(filename):

    # load saved data
    saved=np.load(filename)
    dim=saved['X'].shape[1]

    # create kernel
    k=GPy.kern.StdPeriodic(input_dim=dim,ARD1=True,ARD2=True)

    # create mean function
    mf=GPy.mappings.Constant(dim,1)

    # create model
    m=GPy.models.GPRegression(saved['X'],saved['Y'],kernel=k,mean_function=mf)

    # set model params
    m[:]=saved['params']
    m.fix()
    m.parameters_changed()

    return m

def f(x):

    # choose model to use:

    model=load_model('../models/model_2D_E0.npz')    

    # prediction:

    prediction=model.predict(np.atleast_2d(x))[0][0][0]
        
    return prediction
