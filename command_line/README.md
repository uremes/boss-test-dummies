## 1 Baseline

The baseline examples baseline\_[task]\_[dimension].in demonstrate how to use the models in a BOSS optimisation run. For a tutorial, see https://cest-group.gitlab.io/boss/tutorials/quickstart_cli.html.

## 2 Warm start

Assume that some input-output data is available that describes a task that is related to our current optimisation task. The data could be collected in a previous optimisation run or other experiments carried out in the same input domain. BOSS can use the related data to warm start optimisation in the current task.

To use the related data, initialise a multi-task BOSS run with custom initialisation data that includes both new input data points to be evaluated with the current task and the available input and output data from the related task. The input points must be presented in the extended format with task index 0 for the current task and task index 1 for the related task.

Minimal modifications compared to a baseline setup:
- add `num_tasks 2` and substitute `userfn` with `userfn_list`
- add the custom initialisation data under RESULTS. Each line must contain information about one data point in the format `x_1 x_2 … x_d 0` (data point to be evaluated) or `x_1 x_2 … x_d 1 y` (data point and observed value) where x_i are the input values where the user function will be or has been evaluated and y is the observed output when available.

See examples warm\_start\_[task]\_[dimension]*

## 3 Multi-task initialisation

BOSS can also acquire initialisation data from several tasks at the same time. This can reduce overall computational cost in optimisation in case previous data is not available but a related task exists that is much cheaper to evaluate than the task we wish to optimise.

Minimal modifications compared to a baseline setup:
- substitute `userfn` with `userfn_list` and present the user functions as a semicolon-separated list. The function we wish to optimise comes first: `userfn_list target_fn.py; related_fn.py`
- substitute `initpts` with `task_initpts` and present initpts to be sampled from each task in the same order as user functions: `task_initpts N_target N_related`

See examples init\_multi\_[task]\_[dimension].in

## 4 Multi-task optimisation

Access to one or more related tasks also makes it possible to use multi-task acquisition rules in optimisation. In this case optimisation decides on each iteration both the input parameters and the task to evaluate. Multi-task acquisition rules are cost-aware acquisition rules that take into account the acquisition cost associated with each task.

The current multi-task acquisition function available in BOSS relies on the user to provide acquisition costs and a maximum cost as input. The maximum cost is used as an additional stopping condition in optimisation. In practice optimisation continues until `iterpts` iterations are finished or until the next evaluation would exceed the cost limit defined with `maxcost`.

Minimal modifications compared to a baseline setup:
- substitute `userfn` with `userfn_list` and present the user functions as a semicolon-separated list. The function we wish to optimise comes first: `userfn_list target_fn.py; related_fn.py`
- choose a multi-task acquisition rule: `acqfn_name elcb_multi`
- add acquisition costs in the same order as user functions: `task_cost cost_target cost_related`
- add `maxcost cost_limit` and increase `iterpts`

See examples multi\_[task]\_[dimension]*
